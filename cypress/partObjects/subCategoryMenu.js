'use strict'

export class SubCategoryMenu {

    selectShowAll() {
        cy.get('div[class*="OffcanvasNavigation__ParentItem"] a[class*="NavigationItem"]')
            .click()
    }

    goBack() {
        cy.get('a[href="/category-overview"]')
            .click()
    }

    selectSubWomenCatByCatAndSubCatName(categoryName, subCategoryName) {
        cy.get(`a[href="/frauen/${categoryName}/${subCategoryName}"]`)
        .click()
    }
    
    selectSubMenCatByCatAndSubCatName(categoryName, subCategoryName) {
        cy.get(`a[href="/maenner/${categoryName}/${subCategoryName}"]`)
        .click()
    }
    
    
    selectSubKidsCatByCatAndSubCatName(categoryName, subCategoryName) {
        cy.get(`a[href="/kinder/${categoryName}/${subCategoryName}"]`)
            .click()
    }
}

export const subCategoryMenu = new SubCategoryMenu()