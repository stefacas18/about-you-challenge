'use strict'

export class Header {

    goToShoppingBag() {
        cy.get('[data-cy-id="HeaderBasketIcon"]')
            .click()
    }

    goToWishList() {
        cy.get('[data-cy-id="HeaderWishlistIcon"]')
            .click()
    }

    searchItem() {
        cy.get('[data-cy-id="HeaderSearchIcon"]')
            .click()
    }

}

export const header = new Header()