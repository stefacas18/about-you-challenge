'use strict'

export class LoginPage {

    loginAsGuestUser(guestUserInfo) {
        cy.get('#mode-registration').should('not.be.visible').check({ force: true }).should('be.checked')
        cy.get('#hasOptedToRemainGuest').should('not.be.visible').check({ force: true }).should('be.checked')
        cy.get('form').eq(0).within(($form) => {
            cy.get('#gender-female').should('not.be.visible').check({ force: true }).should('be.checked')
            cy.get('[name="email"]')
                .clear()
                .type(guestUserInfo.email)
            cy.get('[name="firstName"]')
                .clear()
                .type(guestUserInfo.first_name)
            cy.get('[name="lastName"]')
                .clear()
                .type(guestUserInfo.last_name)
            cy.root().submit()
        })
    }
    
    loginExistingUser(existingUserInfo) {
        cy.get('#mode-login').should('not.be.visible').check({ force : true}).should('be.checked')
        cy.get('form').eq(0).within(($form) => {
            cy.get('[name="email"]')
                .clear()
                .type(existingUserInfo.email)
            cy.get('[name="password"]')
                .clear()
                .type(existingUserInfo.password)
            cy.root().submit()
        })
        
    }


}

export const loginPage = new LoginPage()