'use strict'

export class ShoppingBagPage {

    goToCheckout() {
        cy.get('[type="cta"]', { timeout : 5000})
            .eq(0)
            .click()
        cy.wait(3000)
    }

}

export const shoppingBagPage = new ShoppingBagPage()