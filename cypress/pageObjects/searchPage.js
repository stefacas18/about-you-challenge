'use strict'

export class SearchPage {

    searchWord(searchWord) {
        cy.get('input[class*="SearchTermInput__Input"]')
            .clear()
            .type(searchWord)
            .type('{enter}')
        return this
    }

}

export const searchPage = new SearchPage()