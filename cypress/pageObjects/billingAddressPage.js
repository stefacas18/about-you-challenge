'use strict'

export class BillingAddressPage {

    setBillingAddressInfo(billingAddressInfo) {
        cy.get('.address-form').within(($form) => {
            cy.get('[name="streetHouseNumber"]')
                .clear()
                .type(billingAddressInfo.street_house_number)
                if(billingAddressInfo.additional != '') {
                    cy.get('[name="additional"]')
                    .clear()
                    .type(billingAddressInfo.additional)
                }
                cy.get('[name="zipCode"]')
                .clear()
                .type(billingAddressInfo.zip_code)
                cy.get('[name="city"]')
                .clear()
                .type(billingAddressInfo.city)
                cy.get('#billingAddressBirthDate.invalid')
                .clear()
                .type(billingAddressInfo.dob)
        })
        cy.get('.billing-address-action button[type="submit"]')
            .click()
    }

}

export const billingAddressPage = new BillingAddressPage()
