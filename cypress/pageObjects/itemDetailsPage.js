'use strict'

export class ItemDetailsPage {

    selectSizeByIndex(index) {
        cy.get('div[data-cy-id*="size"].iBcUtF')
            .eq(1)
            .click()
    }

    selectSizeBySize(size) {
        cy.get(`[data-cy-id="size_${size}"].iBcUtF`)
            .click()
    }

    addItemToShoppingBag() {
        cy.get('[data-cy-id="AddToBasketButton"]')
            .click()
    }
}

export const itemDetailsPage = new ItemDetailsPage()