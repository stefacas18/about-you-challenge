'use strict'

import paymentMethods from '../fixtures/paymentMethods'
import carries from '../fixtures/carries'

export class YourOrderPage {

    selectCarrierByName(shipWithName) {
        cy.get(`#${(carries[shipWithName]).id}`)
            .should('not.be.visible')
            .check({ force: true })
    }

    selectPaymentMethodByName(paymentMethodName) {
        cy.get(`#${(paymentMethods[paymentMethodName]).id}`)
            .should('not.be.visible').
            check({ force: true })
            .should('be.checked')
    }
    
    submitDeliveryAndPaymentOptions() {
        cy.get('.order-submit button[type="submit"]')
            .click()
    }

}

export const yourOrderPage = new YourOrderPage()