'use strict'

export class ItemListPage {

    selectItemByIndex(index) {
        cy.get('div[class*="ProductTile__Tile"]')
            .eq(index)
            .click()
    }

}

export const itemListPage = new ItemListPage()