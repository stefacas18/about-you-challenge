'use strict'

import { subCategoryMenu } from '../partObjects/subCategoryMenu';
import { header } from '../partObjects/header';

export class HomePage {

    go() {
        cy.visit('/', { timeout: 50000, onBeforeLoad: win => { win.sessionStorage.clear()}})
    }

    selectWomenOpt() {
        cy.get('a[href="/frauen"]')
            .click()
    }

    selectMenOpt() {
        cy.get('a[href="/maenner"]')
            .click()
    }

    selectKidsOpt() {
        cy.get('a[href="/kinder"]')
            .click()
    }

    selectWomenCatByName(categoryName) {
        cy.get(`a[href="/category-overview/frauen/${categoryName}"]`)
            .click()
    }

    selectMenCatByName(categoryName) {
        cy.get(`a[href="/category-overview/maenner/${categoryName}"]`)
            .click()
    }

    selectKidsCatByName(categoryName) {
        cy.get(`a[href="/category-overview/kinder/${categoryName}"]`)
            .click()
    }

    subCategoryMenu() {
        return subCategoryMenu;
    }

    header() {
        return header;
    }

}

export const homePage = new HomePage()