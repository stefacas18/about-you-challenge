// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
module.exports = (on, config) => {
  on('before:browser:launch', (browser = {}, args) => {
    if (browser.name === 'chrome') {
      args.push('--disable-background-networking')
      args.push('--ash-copy-host-background-at-boot')
      args.push('--deterministic-fetch')
      args.push('--force-presentation-receiver-for-testing')
      return args
    }

    if (browser.name === 'electron') {
      return args
    }
  })
}
