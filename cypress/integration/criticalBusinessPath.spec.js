'use strict'

import { homePage } from '../pageObjects/homePage'
import { loginPage } from '../pageObjects/loginPage'
import { itemListPage } from '../pageObjects/itemListPage'
import { itemDetailsPage } from '../pageObjects/itemDetailsPage'
import { shoppingBagPage } from '../pageObjects/shoppingBagPage'
import { billingAddressPage } from '../pageObjects/billingAddressPage'
import { yourOrderPage } from '../pageObjects/yourOrderPage'
import { searchPage } from '../pageObjects/searchPage'

describe('Critical Business Path', function () {
    
    beforeEach(() => {
        cy.viewport('iphone-6+') 
        homePage.go()
        
        cy.server()
        cy.route('/api/outfits/v1/recommendations/category/*').as('itemList')
        cy.route('GET', '/api/hreflang/by_product/*').as('itemDetails')
        cy.route('GET', 'https://checkout-m.aboutyou.de/api/address/delivery/options').as('billingAddress')
        cy.route('GET', '/api/basket/*').as('getShoppingBag')
        cy.route('GET', 'https://checkout-m.aboutyou.de/api/order/carrier').as('carriers')
        cy.route('GET', 'https://api-cloud.aboutyou.de/v1/categories*').as('searchItemList')

        cy.fixture('users').as('users')
    })

    it('Successful checkout as guest user', function () {

        homePage.selectWomenOpt()
        homePage.selectWomenCatByName('sale')
        cy.url().should('include', '/category-overview/frauen/sale')
        homePage.subCategoryMenu().selectSubWomenCatByCatAndSubCatName('sale','schuhe')
        cy.url().should('include', '/frauen/sale/schuhe')
        cy.wait('@itemList')
        itemListPage.selectItemByIndex(0)
        cy.wait('@itemDetails').its('status').should('eq', 200)
        itemDetailsPage.selectSizeByIndex(1)
        itemDetailsPage.addItemToShoppingBag()
        homePage.header().goToShoppingBag()
        cy.wait('@getShoppingBag', {timeout : 10000}).its('status').should('eq', 200)
        shoppingBagPage.goToCheckout()
        loginPage.loginAsGuestUser(this.users.guest)
        cy.wait('@billingAddress').its('status').should('eq', 204)
        billingAddressPage.setBillingAddressInfo({ ... this.users.guest.billing_adress, "dob" : this.users.guest.dob})
        cy.wait('@carriers').its('status').should('eq', 200)
        yourOrderPage.selectCarrierByName('hermes')
        yourOrderPage.selectPaymentMethodByName('master_card')
        yourOrderPage.submitDeliveryAndPaymentOptions()
        cy.url().should('include', 'https://www.computop-paygate.com')
        cy.title().should('eq', 'Kartenzahlung')
        
    })
    
    it('Successful checkout as guest user with different carrier and payment method', function () {
        
        homePage.header().searchItem()
        searchPage.searchWord('boohoo')
        cy.wait('@searchItemList').its('status').should('eq', 200)
        itemListPage.selectItemByIndex(0)
        cy.wait('@itemDetails').its('status').should('eq', 200)
        itemDetailsPage.selectSizeByIndex(1)
        itemDetailsPage.addItemToShoppingBag()
        homePage.header().goToShoppingBag()
        cy.wait('@getShoppingBag', {timeout : 10000}).its('status').should('eq', 200)
        shoppingBagPage.goToCheckout()
        loginPage.loginAsGuestUser(this.users.guest)
        cy.wait('@billingAddress').its('status').should('eq', 204)
        billingAddressPage.setBillingAddressInfo({ ... this.users.guest.billing_adress, "dob" : this.users.guest.dob})
        cy.wait('@carriers').its('status').should('eq', 200)
        yourOrderPage.selectCarrierByName('dhl')
        yourOrderPage.selectPaymentMethodByName('visa')
        yourOrderPage.submitDeliveryAndPaymentOptions()
        cy.url().should('include', 'https://www.computop-paygate.com')
        cy.title().should('eq', 'Kartenzahlung')
        
    })

})
