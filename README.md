# about-you-challenge

The repository has the implementation of two critical business paths as guest user, and also We could do in the future more combinations of paths to successful checkout like select different categories, other carrier or payment method, maybe login as existing user, I didn't implement paths with new, Facebook and existing users, given that the suite runs against production environment.

I used POM (Page Object Model) to make code clear and write test scenarios in a few lines, and if We need to change selectors or actions We only need to do it in one place.

The suite runs in a resolution for Iphone 6+.

The suite has 2 reports JSON mochawesome.json (reports/mochawesome) and HMTL mochawesome-report (mochawesome-report/mochawesome.html)

## install npm
https://www.npmjs.com/get-npm

# open a terminal and follow the following steps

## clone this repo to a local directory 
git clone https://stefacas18@bitbucket.org/stefacas18/about-you-challenge.git
 
## cd into the cloned repo 
cd about-you-challenge
 
## install the node_modules 
npm install
 
## run the suite in Electron (Headless)
npm run cy:run

## run the suite in Chrome
npm run cypress:open 


